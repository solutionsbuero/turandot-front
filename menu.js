const isMac = process.platform === 'darwin';
let mainWindow = null;

module.exports = {

    setMainWindow: function(mw) {
        mainWindow = mw;
    },

    template: [
        // { role: 'appMenu' }
        ...(isMac ? [{
            label: 'System',
            submenu: [
                { role: 'about' },
                { type: 'separator' },
                { role: 'services' },
                { type: 'separator' },
                { role: 'hide' },
                { role: 'hideothers' },
                { role: 'unhide' },
                { type: 'separator' },
                { role: 'quit' }
            ]
        }] : []),
        {
            label: 'Turandot',
            submenu: [
                {
                    label: 'Converter',
                    click:  function(){ mainWindow.webContents.executeJavaScript("openTab('converter')")}
                },
                {
                    label: 'Templates',
                    click:  function(){ mainWindow.webContents.executeJavaScript("openTab('templates')")}
                },
                {
                    label: 'Citation styles',
                    click:  function(){ mainWindow.webContents.executeJavaScript("openTab('csl')")}
                },
                {
                    label: 'Settings',
                    click:  function(){ mainWindow.webContents.executeJavaScript("openTab('settings')")}
                },
                {
                    label: 'Logs',
                    click:  function(){ mainWindow.webContents.executeJavaScript("openTab('logs')")}
                },
                {type: 'separator'},
                isMac ? { role: 'close' } : { role: 'quit' }
            ]
        },
        {
            label: 'View',
            submenu: [
                { role: 'reload' },
                { role: 'forcereload' },
                { role: 'toggledevtools' },
                { type: 'separator' },
                { role: 'resetzoom' },
                { role: 'zoomin' },
                { role: 'zoomout' },
                { type: 'separator' },
                { role: 'togglefullscreen' }
            ]
        },
        {
            role: 'help',
            submenu: [
                {
                    label: 'Learn More',
                    click: async () => {
                        const { shell } = require('electron')
                        await shell.openExternal('https://gitlab.com/solutionsbuero/turandot-meta')
                    }
                }
            ]
        }
    ]
};
