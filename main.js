// Modules to control application life and create native browser window
const {Menu, app, BrowserWindow} = require('electron');
const children = require('./children');
const rp = require("request-promise");
const $ = require("jquery");
let menu = require('./menu');


let mainWindow = null;
let mainAddr = 'http://localhost:61622';


function createWindow () {

    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    menu.setMainWindow(mainWindow);
    const mtemplate = Menu.buildFromTemplate(menu.template);
    Menu.setApplicationMenu(mtemplate);

    mainWindow.loadURL(mainAddr);
    mainWindow.on('closed', function () {
        mainWindow = null
    })
}

app.on("ready", function() {

    children.runCiteproc();
    children.runFlask();

    var StartUp = function() {
        // Promise-ified check if Flask backend is running
        rp( mainAddr )
        .then( // Request promise resolved, start frontend
            function( htmlString ) {
                createWindow();
            }
        )
        .catch( // Request promise rejected, retry
            function( err ) {
                // Only recurse on specific Connection Refused error
                if(err.error != undefined && err.error.code == "ECONNREFUSED") {
                    // Retry
                    StartUp();
                }
            }
        );
    };

    StartUp();
});

app.on("browser-window-created", function(e, window) {
    const mtemplate = Menu.buildFromTemplate(menu.template);
    Menu.setApplicationMenu(mtemplate);
});


// Quit when all windows are closed.
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})
/*
app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow();
    if (subcite === null) children.runCiteproc();
    if (subflask === null) children.runFlask();
})
*/
app.on("quit", function() {
    children.killFlask();
    children.killCiteproc();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
