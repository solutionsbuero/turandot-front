#!/usr/bin/env bash

# source, path, size
svg-to-png () {
    inkscape -z -e $2/$3.png -w $3 -h $3 $1
}


# ico
svg-to-png turandot.svg . 128
convert 128.png Icon.ico
rm 128.png

svg-to-png turandot_one-color.svg base 16
svg-to-png turandot_one-color.svg base 24
svg-to-png turandot_one-color.svg base 32
svg-to-png turandot_one-color.svg base 48
svg-to-png turandot_one-color.svg base 64

svg-to-png turandot.svg linux 128
svg-to-png turandot.svg linux 256
svg-to-png turandot.svg linux 512
svg-to-png turandot.svg linux 1024

svg-to-png turandot.svg mac 128
svg-to-png turandot.svg mac 256
svg-to-png turandot.svg mac 512
svg-to-png turandot.svg mac 1024