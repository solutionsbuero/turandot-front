const path = require('path');
const { execFile } = require('child_process');
const fs = require('fs')

let subflask;
let subcite;

const FLASK_FOLDER = "turandot-srv";
const FLASK_EXEC = "turandot-srv";
const CITE_FOLDER = "citeproc-js-server";
const CITE_FILE = "citeproc"

function getExecPath(efolder, efile) {
    basepath = path.join(__dirname, efolder);
    execfile = path.join(basepath, efile);
    if (process.platform === "win32") {
        execfile += ".exe";
    }
    try {
        if (fs.existsSync(execfile)) {
            console.log("Executable found:");
            console.log(execfile);
        }
    }
    catch(err) {
        console.error(err)
    }
    return [execfile, basepath]
}

function getFlaskPath() {
    return getExecPath(FLASK_FOLDER, FLASK_EXEC);
}

function getCitePath() {
    return getExecPath(CITE_FOLDER, CITE_FILE);
}

module.exports = {

    runFlask: function() {
        fpath = getFlaskPath()
        subflask = execFile(fpath[0], {
            cwd: fpath[1]
        }, function(error, stdout, stderr) {});
        subflask.stdout.on('data', (data) => {
            console.log(data.toString());
        });
        subflask.stderr.on('data', (data) => {
            console.error(data.toString());
        });
    },

    runCiteproc: function() {
        cpath = getCitePath();
        subcite = execFile(cpath[0], {
            cwd: cpath[1]
        }, function(error, stdout, stderr) {});
        subcite.stdout.on('data', (data) => {
            console.log(data.toString());
        });
        subcite.stderr.on('data', (data) => {
            console.error(data.toString());
        });
    },

    killFlask: function () {
        subflask.kill();
    },

    killCiteproc: function() {
        subcite.kill();
    }

}
